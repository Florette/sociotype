import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Groups from './views/Groups'
import Type from "./views/Type";
import Test from "./views/Test";

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/groups',
            name: 'groups',
            component: Groups
        },
        {
            path: '/type/:id',
            name: 'type',
            component: Type,
            props: true
        },
        {
            path: '/test',
            name: 'test',
            component: Test
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve({x: 0, y: 0})
            })
        })
    }
})
