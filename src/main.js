import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import sociotypes from './json/sociotypes';
import groups from './json/groups'
import functions from './json/functions'

Vue.config.productionTip = false;

Vue.mixin({
  computed: {
    $sociotypes() {
      return sociotypes
    },
    $groups() {
      return groups
    },
    $functions() {
      return functions
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
